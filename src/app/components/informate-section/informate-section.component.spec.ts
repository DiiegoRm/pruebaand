import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InformateSectionComponent } from './informate-section.component';

describe('InformateSectionComponent', () => {
  let component: InformateSectionComponent;
  let fixture: ComponentFixture<InformateSectionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InformateSectionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InformateSectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
