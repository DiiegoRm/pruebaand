import { Component, OnInit } from '@angular/core';
import { InformateServiceService } from '../../services/informate-service.service';

@Component({
  selector: 'app-informate-section',
  templateUrl: './informate-section.component.html',
  styleUrls: ['./informate-section.component.css']
})
export class InformateSectionComponent implements OnInit {

  /*properties*/
  public listInfo: any[] = [];
  constructor(private InformateServiceService: InformateServiceService) { }

  ngOnInit(): void {
    this.InformateServiceService.getListInformate().subscribe( resp => {
      this.listInfo = resp;
    });

  }

}
