import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InteresSectionComponent } from './interes-section.component';

describe('InteresSectionComponent', () => {
  let component: InteresSectionComponent;
  let fixture: ComponentFixture<InteresSectionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InteresSectionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InteresSectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
