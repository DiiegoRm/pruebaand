import { Component, OnInit } from '@angular/core';
import { InformateServiceService } from '../../services/informate-service.service';

@Component({
  selector: 'app-interes-section',
  templateUrl: './interes-section.component.html',
  styleUrls: ['./interes-section.component.css']
})
export class InteresSectionComponent implements OnInit {
  
  /*properties*/
  public listInfo: any[] = [];
  
  constructor( private InformateServiceService: InformateServiceService) { }

  ngOnInit(): void {
    this.InformateServiceService.getListOtros().subscribe( resp => {
      //this.listOtros = resp;
    });
  }

}
