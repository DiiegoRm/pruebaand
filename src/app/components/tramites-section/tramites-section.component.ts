import { Component, OnInit } from '@angular/core';
import { InformateServiceService } from '../../services/informate-service.service';

@Component({
  selector: 'app-tramites-section',
  templateUrl: './tramites-section.component.html',
  styleUrls: ['./tramites-section.component.css']
})
export class TramitesSectionComponent implements OnInit {
  /* properties */
  public listProcedure: any[] = [];
  constructor(private InformateServiceService: InformateServiceService) { }

  ngOnInit(): void {
    this.InformateServiceService.getListProcedure().subscribe( resp => {
      this.listProcedure = resp;
    });

  }

}
