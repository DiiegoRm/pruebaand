import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TramitesSectionComponent } from './tramites-section.component';

describe('TramitesSectionComponent', () => {
  let component: TramitesSectionComponent;
  let fixture: ComponentFixture<TramitesSectionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TramitesSectionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TramitesSectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
