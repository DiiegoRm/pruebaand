import { Component, OnInit } from '@angular/core';
import { InformateServiceService } from '../../services/informate-service.service';

@Component({
  selector: 'app-contacto-section',
  templateUrl: './contacto-section.component.html',
  styleUrls: ['./contacto-section.component.css']
})
export class ContactoSectionComponent implements OnInit {
  /* propiedades */
  public listOpinion: any[] = [];
  
  constructor( private InformateServiceService:InformateServiceService) { }

  ngOnInit(): void {
    this.InformateServiceService.getListOpinion().subscribe( resp => {
      this.listOpinion = resp;
    });
  }

}
