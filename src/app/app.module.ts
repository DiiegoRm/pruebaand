import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
/* importacion del http */
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { ContainerComponent } from './components/container/container.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { InformateSectionComponent } from './components/informate-section/informate-section.component';
import { ContactoSectionComponent } from './components/contacto-section/contacto-section.component';
import { InteresSectionComponent } from './components/interes-section/interes-section.component';
import { TramitesSectionComponent } from './components/tramites-section/tramites-section.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    ContainerComponent,
    NavbarComponent,
    InformateSectionComponent,
    ContactoSectionComponent,
    InteresSectionComponent,
    TramitesSectionComponent
  ],
  imports: [
    HttpClientModule,
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
