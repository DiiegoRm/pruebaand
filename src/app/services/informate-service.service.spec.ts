import { TestBed } from '@angular/core/testing';

import { InformateServiceService } from './informate-service.service';

describe('InformateServiceService', () => {
  let service: InformateServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(InformateServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
