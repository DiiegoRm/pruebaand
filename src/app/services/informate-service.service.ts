import { Injectable } from '@angular/core';
/*Import para el service*/
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
/**/

@Injectable({
  providedIn: 'root'
})
export class InformateServiceService {

  constructor(private http: HttpClient) { }

	getListOtros() {
		return this.http.get(`../assets/json/otros.json`).pipe(
			map(resp => this.crearArreglo(resp))
		);
	}
	getListInformate() {
		return this.http.get(`../assets/json/informate.json`).pipe(
			map(resp => this.crearArreglo(resp))
		);
	}
	getListOpinion() {
		return this.http.get(`../assets/json/opinion.json`).pipe(
			map(resp => this.crearArreglo(resp))
		);
	}
	getListProcedure() {
		return this.http.get(`../assets/json/tramites.json`).pipe(
			map(resp => this.crearArreglo(resp))
		);
	}

	private crearArreglo(arregloObj: object) {
		const coleccion: any[] = [];
		Object.keys(arregloObj).forEach(key => {
			const elemento: any = arregloObj[key];
			coleccion.push(elemento);
		})

		if (arregloObj === null) { 
			return []; 
		}
		return coleccion;
	}
}
